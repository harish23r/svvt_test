# I acknolowdge that this bucket sort method has been taken from Github

def bucketSort(A):
    if len(A) == 0:
        return A
    if len(A) == 1:
        return A
    i = 1
    while i < len(A):
        k = A[i]
        j = i - 1
        while j >= 0 and A[j] > k:
            A[j+1] = A[j]
            A[j] = k
            j -= 1
        i += 1
    return A

# A = [5,4,3,2,1,9,8,7,6]
# bucketSort(A)
# print(A)