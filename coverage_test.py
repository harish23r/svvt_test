import main
import coverage

cov = coverage.Coverage()
cov.start()

main.bucketSort([5,4,3,2,1,7,8,9,10])
main.bucketSort([10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
main.bucketSort([-1, -2, -3, -4, -5, -6, -7, -8, -9, -10])
main.bucketSort([-1, -2, -3, 4, -5, 6, -7, 8, -9, 10])
main.bucketSort([1])
# main.bucketSort([])

cov.stop()
cov.save()
cov.html_report()


