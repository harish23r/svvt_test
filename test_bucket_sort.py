from unittest import TestCase
from main import bucketSort


class Test_bucket_sort(TestCase):
    def testRandomInput(self):
        self.assertEqual(bucketSort([23, 56, 12, 78, 24, 89, 52, 48]), [12, 23, 24, 48, 52, 56, 78, 89])
        self.assertEqual(bucketSort([5, 8, 4, 2, 3, 10, 1, 9, 7, 6]), [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    def testDescendingOrder(self):
        self.assertEqual(bucketSort([10, 9, 8, 7, 6, 5, 4, 3, 2, 1]), [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        self.assertEqual(bucketSort([89, 75, 63, 51, 47, 32, 26, 18, 6]), [6, 18, 26, 32, 47, 51, 63, 75, 89])

    def testAscendingOrder(self):
        self.assertEqual(bucketSort([1, 4, 7, 9, 12, 15, 18, 19, 25]), [1, 4, 7, 9, 12, 15, 18, 19, 25])
        self.assertEqual(bucketSort([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]), [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    def testOneInput(self):
        self.assertEqual(bucketSort([5]), [5])
        self.assertEqual(bucketSort([6]), [6])

    def testZeroInput(self):
        self.assertEqual(bucketSort([]), [])

    def testNegativeInput(self):
        self.assertEqual(bucketSort([-1, -2, -3, -4, -5, -6, -7, -8, -9, -10]),
                         [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1])
        self.assertEqual(bucketSort([-23, -45, -57, -68, -72, -83, -96]), [-96, -83, -72, -68, -57, -45, -23])

    def testMixedInput(self):
        self.assertEqual(bucketSort([-1, -2, -3, 4, -5, 6, -7, 8, -9, 10]), [-9, -7, -5, -3, -2, -1, 4, 6, 8, 10])
        self.assertEqual(bucketSort([-23, -45, 57, -68, 72, -83, -96]), [-96, -83, -68, -45, -23, 57, 72])
